<!--HEVEA command line is: hevea intro.tex -->
<!--CUT STYLE article--><!--CUT DEF section 1 --><p>The correspondence between natural-deduction proofs of propositional
intuitionistic logic, usually written as (logic) derivations for
judgments of the form &#x393; &#x22A2; <span style="font-style:italic">A</span>, and well-typed terms in the
simply-typed lambda-calculus, with (typing) derivations for the
judgment &#x393; &#x22A2; <span style="font-style:italic">t</span> : <span style="font-style:italic">A</span>, is not one-to-one. In typing judgments
&#x393; &#x22A2; <span style="font-style:italic">t</span> : <span style="font-style:italic">A</span>, the context &#x393; is a mapping from free
variables to their type. In logic derivations, the context &#x393; is
a set of hypotheses; there is no notion of variable, and at most one
hypothesis of each type in the set. This means, for example, that the
following logic derivation
</p><div class="mathpar">
<table class="mprow"><tr style="vertical-align:middle"><td class="mprcell"  style="text-align:center"><table class="display dcenter"><tr style="vertical-align:middle"><td class="dcell">&#xA0;&#xA0;</td><td class="dcell"><table style="border:0;border-spacing:1px;border-collapse:separate" class="cellpadding0"><tr style="vertical-align:bottom"><td style="whitespace:nowrap;text-align:center"><table style="border:0;border-spacing:1px;border-collapse:separate" class="cellpadding0"><tr style="vertical-align:bottom"><td style="whitespace:nowrap;text-align:center"><table style="border:0;border-spacing:1px;border-collapse:separate" class="cellpadding0"><tr style="vertical-align:bottom"><td style="whitespace:nowrap;text-align:center">&#xA0;</td></tr>
<tr><td style="background-color:green;height:3px"></td></tr>
<tr style="vertical-align:top"><td style="whitespace:nowrap;text-align:center"><span style="font-style:italic">A</span>&#xA0;&#x22A2;&#xA0;<span style="font-style:italic">A</span></td></tr>
</table></td></tr>
<tr><td style="background-color:green;height:3px"></td></tr>
<tr style="vertical-align:top"><td style="whitespace:nowrap;text-align:center"><span style="font-style:italic">A</span>&#xA0;&#x22A2;&#xA0;<span style="font-style:italic">A</span>&#xA0;&#x2192;&#xA0;<span style="font-style:italic">A</span></td></tr>
</table></td></tr>
<tr><td style="background-color:green;height:3px"></td></tr>
<tr style="vertical-align:top"><td style="whitespace:nowrap;text-align:center">&#x2205;&#xA0;&#x22A2;&#xA0;<span style="font-style:italic">A</span>&#xA0;&#x2192;&#xA0;<span style="font-style:italic">A</span>&#xA0;&#x2192;&#xA0;<span style="font-style:italic">A</span></td></tr>
</table></td></tr>
</table></td></tr>
</table></div><p>
corresponds to two <em>distinct</em> programs, namely
&#x3BB; (<span style="font-style:italic">x</span>)&#xA0;&#x3BB; (<span style="font-style:italic">y</span>)&#xA0;<span style="font-style:italic">x</span> and &#x3BB; (<span style="font-style:italic">x</span>)&#xA0;&#x3BB; (<span style="font-style:italic">y</span>)&#xA0;<span style="font-style:italic">y</span>. We
say that those programs have the same <em>shape</em>, in the sense that
the erasure of their typing derivation gives the same logic
derivation &#x2013; and they are the only programs of this shape.</p><p>Despite, or because, not being one-to-one, this correspondence is very
helpful to answer questions about type systems. For example, the
question of whether, in a given typing environment &#x393;, the type
<span style="font-style:italic">A</span> is inhabited, can be answered by looking instead for a valid
logic derivation of &#x230A;&#x393;&#x230B; &#x22A2; <span style="font-style:italic">A</span>, where &#x230A;&#x393;&#x230B;
denotes the erasure of the mapping &#x393; into a set of hypotheses. If
we can independently prove that only a finite number of different
types need to be considered to find a valid proof (this is the case
for propositional logic because of the <em>subformula property</em>),
then there are finitely many set-of-hypothesis &#x394;, and the
search space of sequents &#x394; &#x22A2; <span style="font-style:italic">B</span> to consider during proof
search is finite. This property is key to the termination of most
search algorithms for the simply-typed lambda-calculus. Note that it
would not work if we searched typing derivations &#x393; &#x22A2; <span style="font-style:italic">t</span> : <span style="font-style:italic">A</span>
directly: even if there are finitely many types of interest, the set
of mappings from variables to such types is infinite.</p><p>In our current brand of work, we are interested in a different
problem. Instead of knowing whether there <em>exists</em> a term <span style="font-style:italic">t</span>
such that &#x393; &#x22A2; <span style="font-style:italic">t</span> : <span style="font-style:italic">A</span>, we want to know whether this term is
<em>unique</em> &#x2013; modulo a given notion of program
equivalence. Intuitively, this can be formulated as a search problem
where search does not stop to the first candidate, but tries to find
whether a second one (that is nonequivalent as a program) exists. In
this setting, the technique of searching for logic derivations
&#x230A;&#x393;&#x230B; &#x22A2; <span style="font-style:italic">A</span> instead is not enough, because a unique logic
derivation may correspond to several distinct programs of this shape:
summarizing typing environments as set-of-hypotheses loses information
about (non)-unicity.</p><p>To better preserve this information, one could keep track of the
number of times an hypothesis has been added to the context,
representing contexts as <em>multisets</em> of hypotheses; given a logic
derivation annotated with such counts in the context, we can precisely
compute the number of programs of this shape. However, even for
a finite number of types/formulas, the space of such multisets is
infinite; this breaks termination arguments. A natural idea is then to
<em>approximate</em> multisets by labeling hypotheses with 0
(not available in the context), 1 (added exactly once), or <span style="text-decoration:overline">2</span>
(available two times <em>or more</em>); this two-or-more approximation
has three possible states, and there are thus finitely many contexts
annotated in this way.</p><p>The question we answer in this note is the following: is the
two-or-more approximation correct? By correct, we mean that if the
<em>precise</em> number of times a given hypothesis is available varies,
but remains in the same approximation class, then the total number of
programs of this shape may vary, but will itself remain in the same
annotation class. A possible counter-example would be a logic
derivation &#x394; &#x22A2; <span style="font-style:italic">B</span> such that, if a given hypothesis
<span style="font-style:italic">A</span> &#x2208; &#x394; is present exactly twice in the context (or has two free
variables of this type), there is one possible program of this shape,
but having three copies of this hypothesis would lead to several distinct
programs.</p><p>Is this approximation correct? We found it surprisingly difficult to
have an intuition on this question (guessing what the answer should
be), and discussions with colleagues indicate that there is no obvious
guess &#x2013; people have contradictory intuitions on this.</p><!--CUT END -->
<!--HTMLFOOT-->
<!--ENDHTML-->
