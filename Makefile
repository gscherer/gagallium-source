STOG=stog --package stog.disqus,stog.markdown,stog-math --nocache
DEST_DIR=../gagallium-output
BASE_URL_OPTION=--site-url http://gallium.inria.fr/blog
PUBLICATION_LEVEL_OPTION=
STOG_OPTIONS=-d $(DEST_DIR) $(BASE_URL_OPTION) $(PUBLICATION_LEVEL_OPTION) --tmpl tmpl $(STOG_VERBOSE)
LESSC=lessc

STOG_TEST_URL ?= http://gallium.inria.fr/~agueneau/gagallium-test

.PHONY: clean build site test

clean:
	rm -fr $(DEST_DIR)/*
	rm -f ocaml.log

build:
	rm -fr $(DEST_DIR)/*
	$(MAKE) site

site:
	time $(STOG) $(STOG_OPTIONS) .
	(cd less && $(LESSC) style.less > style.css)
	cp less/style.css $(DEST_DIR)/
	cp -f *.png tmpl/*png $(DEST_DIR)/ || true
	cp -f *.svg $(DEST_DIR)/ || true

test:
	rm -fR ../gagallium-test/*
	$(MAKE) BASE_URL_OPTION="--site-url $(STOG_TEST_URL)" PUBLICATION_LEVEL_OPTION="--publication-level draft" DEST_DIR="../gagallium-test" build
	cp ../test-htaccess ../gagallium-test/.htaccess

local:
	$(MAKE) BASE_URL_OPTION="--site-url file:///tmp/gagallium" PUBLICATION_LEVEL_OPTION="--publication-level draft" DEST_DIR="/tmp/gagallium" build
