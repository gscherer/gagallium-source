(*
   ocamlfind ocamlc -pp camlp4rf -package camlp4 -c add_smartcatch.ml

   camlp4o add_smartcatch.cmo -str "let () = try raise Exit with Exit -> print_newline ()"
*)
open Camlp4;

module Id = struct
  value name    = "JHwith";
  value version = "0.1";
end;

module Make (AstFilters : Camlp4.Sig.AstFilters) = struct
  open AstFilters;
  open Ast;

  value add_debug_expr _loc patvar e =
    <:expr<
        (* put your stuff here *)
        let _ = ExceptionHandling.register $lid:patvar$ in
        $e$
    >>;

  value rec map_handler =
    let patvar = "__exn" in
    fun
    [ <:match_case@_loc< $m1$ | $m2$ >> ->
        <:match_case< $map_handler m1$ | $map_handler m2$ >>
    | <:match_case@_loc< $p$ when $w$ -> $e$ >> ->
        <:match_case@_loc<
          ($p$ as $lid:patvar$) when $w$ -> $add_debug_expr _loc patvar e$ >>
    | m -> m ];

  value filter = object
    inherit Ast.map as super;
    method expr = fun
    [ <:expr@_loc< try $e$ with [ $h$ ] >> ->
      <:expr< try $e$ with [ $map_handler h$ ] >>
    | x -> super#expr x ];
  end;

  register_str_item_filter filter#str_item;

end;

let module M = Camlp4.Register.AstFilter Id Make in ();
