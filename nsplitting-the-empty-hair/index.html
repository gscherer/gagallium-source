<post
   title="N-splitting the empty hair"
   date="2013/07/04"
   published="draft"
   author="Gabriel Scherer"
   topics="library design"
   keywords="nsplit,batteries,specification"

   disqus-id="nsplitting-the-empty-hair"
   rss-id="http://gallium.inria.fr/blog/nsplitting-the-empty-hair"
>

<markdown command="pandoc">
Consider a `nsplit : ~sep:string -> string -> string list` function such as
`nsplit ~sep:"toto" "tata,toto,tutu"` returns `["tata,"; ",tutu"]`. What
should the result of `nsplit ~sep:"toto" ""` be?
</markdown>

<sep_/>

<markdown command="pandoc"
args="--base-header-level=3"><![CDATA[
Splitting the empty string (or list) is a hard question. Here are some
easier questions that you may want to consider before reading further
in this post:

- `nsplit ~sep:";" "a;"`?
- `nsplit ~sep:";" ";"`?
- `nsplit ~sep:";" ";"`?

# What do we want?

We want to pick a precise specification to give to the users of the
library. Everyone agrees with the easy case where each occurence of
the separator is surrounded by lots of non-separator stuff. But the
difficulty is to know what to do in the other cases -- edge cases.

Besides, you *must* specify what the behavior is in the edge cases,
because your users will think that you have even if you haven't. If an
user isn't sure about what a function does, he or she will not read
the documentation, but *test* this edge case. Once the test result is
printed on the screen, it will remain indefinitely inprinted in the
user memory as "what the function does", and you cannot change the
behavior (even the part that was not specified) anymore -- because
maintaining compatibility is not about who is right or wrong about
what was guaranteed in the documentation, but which amount of actual
code you break.

So you have to make a choice (maybe you'll just pick the one that is
the most natural to implement), and you have to explain it in the
specification; so it should rather be easy to specify precisely.

# We want a theorem

In those case, it is often fruitful to look for a *theorem* about the
function you want to specify. Said otherwise: "what's the best
QuickCheck test one could write for this function?".

For `nsplit`, there is a rather natural candidate: we could talk about
how to get back the input chain, starting from the result of
`nsplit`. This is exactly what the
`join : string -> string list -> string` does:
`join "toto" ["tata,";",tutu"]` is `"tata,toto,tutu"`.

One theorem expressing the relation between `nsplit` and `join` is the
following:

    ∀ sep input,    join sep (nsplit sep input) = input

This allows us to answer most of the questions about `nsplit` we had so far:

- `nsplit ~sep:"toto" "tata,toto"` must be `["tata,"; ""]` and not
  `["tata,"]`, because `join "toto" ["tata"]` is just `"tata"`
- `nsplit ~sep:"toto" "toto"` must be `[""; ""]`
- `nsplit ~sep:"toto" "totototo"` must be `[""; ""; ""]`

Sadly, there are still two possibilities for `nsplit ~sep:"toto" ""`, as
both `[]` and `[""]` are joined back into the empty string.

# Only one must remain

It is interesting to notice that, for any non-empty input `s`, the
result of `nsplit sep s` is neither `[]` nor `[""]`. This means that,
whatever choice we make about the result for the empty string, the
other possibility will never appear as the result of a `nsplit`
call. The result space of the `nsplit sep` function is not the set of
lists of strings that do not contain `sep`, but this set minus one of
those two elements. Do we have a good reason to exclude one rather
than another?

I think that excluding `[]` is the better choice, because it gives
a clear invariant: the result of `nsplit` is always a non-empty list.

# `nsplit` in context

In the result list of `nsplit`, the first and the last elements in the
list have a special status. Between any two consecutive elements, we
know that there was a separator in the input string. But before the
first, and after the last element, there was nothing in the input
string. If our input was part of a larger string, the extremal
substrings could have been surrounded by separators, or not. What is
the relation between `nsplit ~sep s` and `nsplit ~sep (s ^ s')`?

This suggests another question: what is the relation between
`nsplit`-ting the concatenation of two strings, and the result of
`nsplit` on those two strings?

For example, `nsplit ~sep:";" ("ab;c" ^ "d;ef")` is `["ab";"cd";"ef"]`,
while `(nsplit ~sep:";" "ab;c", nsplit ~sep:";" "d;ef")` is `(["ab";"c"],
["d";"ef"])`. It is pretty clear how to go from the two results to the
larger one: we concatenate the two lists, but have to glue the last
result of the first list and the first result of the last. Note that
this wouldn't work at all if we defined `nsplit ~sep:";" "ab;"` to be just
`["ab"]` – so our previous `join` intuition still makes sense in that
context.

We can write a `merge` function that combines results of the `nsplit`
function – assuming our invariant on `nsplit` result lists that they
are always non-empty.

    let rec merge sep la lb = match la, lb with
      | [a], b::lb -> nsplit ~sep (a ^ b) @ lb
      | a::(_::_ as la), lb -> a :: merge sep la lb

The last component of the first list is merged to the first component
of the last list in the first case. We call `nsplit` again in case
merging the two ends created a new separator – think of
`nsplit ~sep:"toto" ("tato" ^ "totu")`.

We now have that

    ∀sep s1 s2, nsplit ~sep (s1 ^ s2) = merge (nsplit ~sep s1) (nsplit ~sep s2)

# Monoids

In mathematical terms, what we have done above is to express `nsplit`
as a morphism between two monoid structures: the strings with the
concatenation operator `(^)`, and (non-empty) string lists with this
new `merge` operation. `nsplit` transports us from one structure to
another, preserving the structure of the concatenation operation.

The empty list is the neutral element of the `(string, (^))` monoid:
for any string `s` we have `s ^ ""` equal to `"" ^ s` equal to
`s`. It is mapped to the empty element in the result monoid, `[""]`.

Note that it would still be possible to set `[]` as the return value
of `nsplit`. We would have to extend `merge` to handle the empty list
case, and `[]` would become the new neutral element. But this purely
adds some code, and doesn't simplify things: the "interesting" case of
`nsplit` must still assume non-empty lists. Again, it is the
naturalness of implementation that provides the decisive argument in
an interface discussion.

# Still unconvinced?

I initially found that argument unconvincing (it looks like we've made
an arbitrary choice between `[]` and `[""]`), but decided to sleep on
it. Then I found two more arguments in favor of `[""]`. I think that's
a property of good solutions: you will reinforce your appreciation of
them in the future. I find that mathematically-inspired considerations
help take a decision that is stable over time, unlike an arbitrary
choice that you may find reasons to regret two releases later.

1. For any input `s` that doesn't contain the separator `sep`, `nsplit
   sep s` is equal to `[s]`.

2. The length of the list returned by `nsplit ~sep` is equal to the
   number of (non-overlapping) occurences of the separator `sep`, plus
   one.
]]></markdown> </post>
