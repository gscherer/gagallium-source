% Working with Grumpy Camels
% Gabriel Scherer
% OCaml Paris Meeting, May 21st

# Generalities

## The OCaml implementation is a free software project

(I'm talking on the compiler, libraries and tools distributed together
on caml.inria.fr)

The usual advice for contribution:

- report bugs
- improve documentation
- propose small independent patches (using the version-control system)
- get in contact with upstream as soon as possible if you with to
  integrate changes

## Core maintainers

They historically come from a specific INRIA team  
... but are now spread among continents and industries!

Most parts of the codebase have one or two specific
maintainers.  
They react to bug reports, and their opinion on
change proposals is generally respected.

***

<!-- HEAD: 2c6d524259bb1b14b1e70570778126fc2001d825 on github,
     trunk@13694 -->

    % git shortlog -n -s HEAD~1000... # Oct. 2011
      305  Damien Doligez
      215  Alain Frisch
      155  Jacques Garrigue
       81  Wojciech Meyer
       78  Xavier Leroy
       46  Fabrice Le Fessant
       38  Benedikt Meurer
       35  Pierre Weis
       29  Xavier Clerc
       26  Jérémie Dimino
       23  Jonathan Protzenko
       15  Gabriel Scherer
       11  Hongbo Zhang
        5  Maxence Guesdon
        2  Luc Maranget
  
## The Bugtracker

Most of the development activity goes through the bugtracker.

[http://caml.inria.fr/mantis/view_all_bug_page.php](http://caml.inria.fr/mantis/view_all_bug_page.php)

(There is a private development mailing-list, mostly review requests
for larger changes from maintainers.)

 

We already do have a lot of external contributors on the bugtracker!

<!-- export bug reports from Mantis, then
# cat bugreports.csv | sort -rn | head -n 500 | cut -d ',' -f 3 | sort | uniq -c | sort -rn | head -n 50 -->

Some examples : Markus Mottl, Leo P. White, Pierre Chambart, Hendrik
Tews, Jun Furuse, Gerd Stolpmann, Daniel Bünzli, ygrek, Jeremy Yallop,
Matej Kosik, Stéphane Glondu, Benoît Vaugon...



# Specifics: language maintainers are *conservative* by experience

## Benevolent part-time hobby

Maintainers get no professional recognition for their work.

 

Researchers rarely get credit for implementing software.  
Never for maintaining it.

 

Think of this as a after-work hobby.  



## Long-time responsability

Maintainers are in charge for a *long* time.

 

First OCaml commit around 1995.

 

Caml as a language starts around 1985.

 

Stuff we add now will still be there in 2025.



## Mistakes are forever

*Very* strict backward-compatibility requirements for the language
(and distributed library).

 

Breaking changes are not accepted.

 

Changes that don't *feel right* are suspicious.




# Life in the bug tracker

## Bugs

Stuff that is broken. Simple, objective. We like bug reports.

You can help by reporting them, adding information to the report
(eg. other related cases that are also broken), suggesting patches to
fix it, or reviewing or suggested patches.

## Feature requests.

Easy to propose, hard and stressful to evaluate.

Most of the time feature requests overlap with existing or desired
features, and don't add enough value to be worth it.

## Optimizations

Changes that allegedly make program faster.

Most of the time, optimizations do great on a well-chosen
microbenchmark, but have a neglectible effects in real programs.

We don't want to make the compiler more complex unless the gain is
real.


## OCamlbuild notice

Some areas of the distribution are younger, so there is more "free
space" for fixes and feature contributions.

 

Bugfixes and contributions for OCamlbuild are desirable and welcome.

 

There are numerous [feature
requests](http://caml.inria.fr/mantis/search.php?project_id=1&category=OCamlbuild%20%28the%20tool%29&sticky_issues=on&sortby=last_updated&dir=DESC&hide_status_id=80)
submitted already. Go see if there is something you want to help
implement.

 

(It's fine if you use another build system.)

 

## Bugtracker summary

There are several ways one can contribute to the development of the
OCaml distribution. They go through the bugtracker:

- report bugs

- signal that you were bitten by an already-reported issue (this helps
  prioritize work)

- propose patches to fix bugs or implement a request

- review proposed patches: read the code to give an opinion, improve
  on the implementation, or test them against your own software

Junior contributor? Follow the bugtracker.


## Or outside the distribution!

OCaml distribution is a small part of the OCaml ecosystem.
There are a lot of (better?) ways to contribute. For example:

- Report precise user-experience problems ([example](https://github.com/protz/ocaml-installer/issues/18))

- Contribute to the library ecosystem;
  [Daniel Bünzli](http://erratique.ch/tags/OCaml),      
  [Markus Mottl](http://ocaml.info/software.html),
  [Gerd Stolpmann](http://projects.camlcity.org/projects)...

- Communicate about your use of the language;
  [Gagallium](http://gallium.inria.fr/blog/), [Incubaid blog](http://blog.incubaid.com/)...

- Help improve the community-maintained website: [ocaml.org](http://ocaml.org/)

- Do whatever you like to do... with OCaml.


## Thanks. Questions?

<!-- pandoc -s --mathml -i -t dzslides ocaml-paris-meeting-may-2013-scherer.text -o ocaml-paris-meeting-may-2013-scherer.html -->
